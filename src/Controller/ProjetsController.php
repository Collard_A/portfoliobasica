<?php
/*
	./src/Controller/ProjetsController.php
*/

namespace App\Controller;

use Ieps\Core\GenericController;
use App\Entity\Projets;
use Symfony\Component\HttpFoundation\Request;

/**
 * [ProjetsController - Controller des projets]
 */
class ProjetsController  extends GenericController {

  /**
   * [indexAction - Liste des projets]
   * @param  Request $request
   * @param  string  $vue     [Nom de la vue à charger, par défaut projets/index.html.twig]
   * @param  array   $orderBy [Tri par date de création par défaut]
   * @param  int     $limit   [Nombre de projets affichés, pas de limite par défaut]
   * @param  int     $offset  [Nombre du décalage de projets à obtenir.]
   * @return array            [projets]
   */
  public function indexAction(Request $request, string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null, int $offset = null){
    $offset = $request->request->get('offset');
    if ($offset != null):
      $limit = 6;
    endif;
    $projets = $this->_repository->findBy([], $orderBy, $limit, $offset);
    return $this->render('projets/'.$vue.'.html.twig',[
      'projets' => $projets
    ]);
  }

  /**
   * [SliderAction - Liste des projets à affichés dans le slider]
   * @param integer $limit [Nombre de projets affichés dans le slider, pas de limite par défaut]
   * @return array         [projets]
   */
  public function SliderAction(int $limit = 3){
    $projets = $this->_repository->findBy(['enVitrine' => 1], ['dateCreation' => 'DESC'], $limit);
    return $this->render('projets/slide.html.twig',[
      'projets' => $projets
    ]);
  }

  /**
   * [relatedAction - Liste des projets avec tag(s) similaire(s) au projet affiché]
   * @param  string $vue     [Nom de la vue à charger, par défaut projets/index.html.twig]
   * @param  int $current    [Id de la page en cours]
   * @return array           [projets]
   */
  public function relatedAction(string $vue = 'index', $current = null){
    $projets = $this->_repository->findBySimilarTags($current);
    return $this->render('projets/'.$vue.'.html.twig',[
      'projets' => $projets
    ]);
  }

}
