<?php
/*
	./src/Controller/PostsController.php
*/

namespace App\Controller;

use Ieps\Core\GenericController;
use App\Entity\Posts;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

/**
 * [PostsController - Controller des posts]
 */
class PostsController  extends GenericController {

  /**
   * [indexAction - Liste des Posts]
   * @param  string $vue     [Nom de la vue à charger, par défaut posts/index.html.twig]
   * @param  array  $orderBy [Tri par date de création par défaut]
   * @param  int $limit      [Nombre de posts affichés, pas de limite par défaut]
   * @return array           [posts]
   */
  public function indexAction(string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null){
    $posts = $this->_repository->findBy([], $orderBy, $limit);
    return $this->render('posts/'.$vue.'.html.twig',[
      'posts' => $posts
    ]);
  }

  /**
   * [paginateAction - Pagination des posts]
   * @param  Request            $request   
   * @param  PaginatorInterface $paginator
   * @return array              [posts]
   */
  public function paginateAction(Request $request, PaginatorInterface $paginator){
    $request = Request::createFromGlobals();
    $query = $this->_repository->findAllQuery();
    $paginate = $paginator->paginate(
      $query,
      $request->query->getInt('page', 1),
      4
    );
    return $this->render('posts/paginate.html.twig',[
      'posts' => $paginate
    ]);
  }

}
