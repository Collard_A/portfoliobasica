<?php
/*
	./src/Controller/CategoriesController.php
*/

namespace App\Controller;

use Ieps\Core\GenericController;
use App\Entity\Categories;
use Symfony\Component\HttpFoundation\Request;

/**
 * [CategoriesController - Controller des categories]
 */
class CategoriesController  extends GenericController {

  /**
   * [indexAction - liste des Catégories]
   * @param  string $vue      [Nom de la vue à charger, par défaut categories/index.html.twig]
   * @param  array  $orderBy  [Tri par date de création par défaut]
   * @param  int    $limit    [Nombre de catégories affichées, pas de limite par défaut]
   * @return array            [pages]
   */

  public function indexAction(string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null){
    $categories = $this->_repository->findBy([], $orderBy, $limit);
    return $this->render('categories/'.$vue.'.html.twig',[
      'categories' => $categories
    ]);
  }

}
