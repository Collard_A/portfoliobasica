<?php
/*
	./src/Controller/PagesController.php
*/

namespace App\Controller;

use Ieps\Core\GenericController;
use App\Entity\Pages;
use Symfony\Component\HttpFoundation\Request;

/**
 * [PagesController - Controller des pages]
 */
class PagesController  extends GenericController {

  /**
   * [indexAction - liste des Pages]
   * @param  int $current [Id de la page en cours]
   * @return array        [pages]
   */
  public function indexAction(int $current = null){
    $pages = $this->_repository->findAll();
    return $this->render('pages/index.html.twig', [
      'pages' => $pages,
      'current' => $current
    ]);
  }

}
