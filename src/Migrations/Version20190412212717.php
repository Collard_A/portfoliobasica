<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190412212717 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, nom_fr VARCHAR(255) NOT NULL, nom_en VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pages (id INT AUTO_INCREMENT NOT NULL, titre_menu_fr VARCHAR(45) NOT NULL, titre_menu_en VARCHAR(45) NOT NULL, titre_fr VARCHAR(45) NOT NULL, titre_en VARCHAR(45) NOT NULL, titre_description_fr VARCHAR(255) DEFAULT NULL, titre_description_en VARCHAR(255) DEFAULT NULL, sous_titre_description_fr VARCHAR(255) DEFAULT NULL, sous_titre_description_en VARCHAR(255) DEFAULT NULL, description_fr LONGTEXT DEFAULT NULL, description_en LONGTEXT DEFAULT NULL, tri INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts (id INT AUTO_INCREMENT NOT NULL, titre_fr VARCHAR(255) NOT NULL, titre_en VARCHAR(255) NOT NULL, titre_texte_lead_fr VARCHAR(255) DEFAULT NULL, titre_texte_lead_en VARCHAR(255) DEFAULT NULL, texte_lead_fr LONGTEXT DEFAULT NULL, texte_lead_en LONGTEXT DEFAULT NULL, titre_texte_suite_fr VARCHAR(255) DEFAULT NULL, titre_texte_suite_en VARCHAR(255) DEFAULT NULL, texte_suite_fr LONGTEXT DEFAULT NULL, texte_suite_en LONGTEXT DEFAULT NULL, image VARCHAR(45) NOT NULL, date_creation DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projets (id INT AUTO_INCREMENT NOT NULL, nom_fr VARCHAR(45) NOT NULL, nom_en VARCHAR(45) NOT NULL, client VARCHAR(45) DEFAULT NULL, description_fr LONGTEXT DEFAULT NULL, description_en LONGTEXT DEFAULT NULL, date_creation DATETIME NOT NULL, image VARCHAR(45) DEFAULT NULL, createur VARCHAR(45) DEFAULT NULL, en_vitrine INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, nom_fr VARCHAR(45) NOT NULL, nom_en VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(45) NOT NULL, pwd VARCHAR(45) NOT NULL, statut INT NOT NULL, nom VARCHAR(45) DEFAULT NULL, prenom VARCHAR(45) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE pages');
        $this->addSql('DROP TABLE posts');
        $this->addSql('DROP TABLE projets');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE users');
    }
}
