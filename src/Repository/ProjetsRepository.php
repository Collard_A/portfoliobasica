<?php

namespace App\Repository;

use App\Entity\Projets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Projets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projets[]    findAll()
 * @method Projets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

/**
 * [ProjetsRepository - Repository des Projets]
 */
class ProjetsRepository extends ServiceEntityRepository {

    /**
     * [__construct]
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Projets::class);
    }

    /**
     * [findBySimilarTags]
     * @param  int $current [Id du projet affiché]
     * @return array        [Projets avec tags similaires triés par nombre de tags en communs]
     */
    public function findBySimilarTags($current) {
      /**
       * [$tags - recherche des tags du projet affiché]
       * @var array [tags]
       */
      $tags = $this->createQueryBuilder('p')
                   ->select('t.id')
                   ->join('p.tags', 't')
                   ->where('p.id = :current')
                   ->setParameter('current', $current)
                   ->getQuery()
                   ->getResult();
      /**
       * [$rs - recherche des projets qui ont des tags similaires triés par nbre de tags en communs]
       * @var array [Projets]
       */
      $rs = $this  ->createQueryBuilder('p')
                   ->join('p.tags', 't')
                   ->where('t.id IN(:tags)')
                   ->andWhere('p.id != :current')
                   ->setParameter('tags', $tags)
                   ->setParameter('current', $current)
                   ->setMaxResults(4)
                   ->groupBy('p.id')
                   ->addOrderBy('COUNT(t.id)', 'DESC')
                   ->addOrderBy('p.id', 'ASC')
                   ->getQuery();
         return $rs->execute();

    }

    // /**
    //  * @return Projets[] Returns an array of Projets objects
    //  */
    /*
    public function findByExampleField($value) {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    */

    /*
    public function findOneBySomeField($value): ?Projets {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
    */
}
