<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetsRepository")
 * @Vich\Uploadable
 */
class Projets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank
     */
    private $nomFr;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank
     */
    private $nomEn;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $client;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionEn;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="projet_images", fileNameProperty="image")
     * @Assert\Image(minWidth = 1200, maxWidth = 1200, minHeight = 750, maxHeight = 750)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $createur;

    /**
     * @ORM\Column(type="integer")
     */
    private $enVitrine = 0;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", inversedBy="projets")
     */
    private $tags;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @var string
     */
    private $slider;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $texteSliderFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $texteSliderEn;

    /**
     * @Vich\UploadableField(mapping="projetSlider_images", fileNameProperty="slider")
     * @Assert\Image(minWidth = 1920, maxWidth = 1920, minHeight = 700, maxHeight = 700)
     * @var File
     */
    private $sliderFile;

    /**
     * [private Slug du titre du menu en Fr]
     * @var [type="text"]
     */
    private $slugFr;

    /**
     * [private Slug du titre du menu en En]
     * @var [type="text"]
     */
    private $slugEn;

    /**
     * [__construct]
     */
    public function __construct() {
        $this->tags = new ArrayCollection();
        $this->dateCreation = new \Datetime();
    }

    /**
     * [__toString- Appel par easyadminBundle]
     * @return string [nomFr]
     */
    public function __toString() {
  		return $this->nomFr;
  	}

    /**
     * [getId]
     * @return int [id des projets]
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * [getNomFr]
     * @return string [nom des projets en Fr]
     */
    public function getNomFr(): ?string {
        return $this->nomFr;
    }

    /**
     * [setNomFr]
     * @param  string $nomFr
     * @return self
     */
    public function setNomFr(string $nomFr): self {
        $this->nomFr = $nomFr;
        return $this;
    }

    /**
     * [getNomEn]
     * @return string [nom des projets en En]
     */
    public function getNomEn(): ?string {
        return $this->nomEn;
    }

    /**
     * [setNomEn]
     * @param  string $nomEn
     * @return self
     */
    public function setNomEn(string $nomEn): self {
        $this->nomEn = $nomEn;
        return $this;
    }

    /**
     * [getClient]
     * @return string [Nom des clients]
     */
    public function getClient(): ?string {
        return $this->client;
    }

    /**
     * [setClient]
     * @param string $client
     * @return self
     */
    public function setClient(?string $client): self {
        $this->client = $client;
        return $this;
    }

    /**
     * [getDescriptionFr]
     * @return string [descriptions des projets en Fr]
     */
    public function getDescriptionFr(): ?string {
        return $this->descriptionFr;
    }

    /**
     * [setDescriptionFr]
     * @param string $descriptionFr
     * @return self
     */
    public function setDescriptionFr(?string $descriptionFr): self {
        $this->descriptionFr = $descriptionFr;
        return $this;
    }

    /**
     * [getDescriptionEn]
     * @return string [descriptions des projets en En]
     */
    public function getDescriptionEn(): ?string {
        return $this->descriptionEn;
    }

    /**
     * [setDescriptionEn]
     * @param string $descriptionEn
     * @return self
     */
    public function setDescriptionEn(?string $descriptionEn): self {
        $this->descriptionEn = $descriptionEn;
        return $this;
    }

    /**
     * [getDateCreation]
     * @return DateTime [Date de creation des projets]
     */
    public function getDateCreation(): ?\DateTimeInterface {
        return $this->dateCreation;
    }

    /**
     * [setDateCreation]
     * @param  DateTimeInterface $dateCreation
     * @return self
     */
    public function setDateCreation(\DateTimeInterface $dateCreation): self {
        $this->dateCreation = $dateCreation;
        return $this;
    }

    /**
     * [setImage]
     * @param string $image
     */
    public function setImage($image) {
        $this->image = $image;
    }

    /**
     * [getImage]
     * @return string [nom de l'image]
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * [setImageFile]
     * @param File $imageFile
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * [getImageFile]
     * @return File $imageFile
     */
    public function getImageFile() {
      return $this->imageFile;
    }

    /**
     * [getUpdatedAt]
     * @return Datetime [date de l'upload des images]
     */
    public function getUpdatedAt() {
      return $this->updatedAt;
    }

    /**
     * [setUpdatedAt]
     * @param DateTime $datetime
     */
    public function setUpdatedAt(\DateTime $datetime) {
      $this->updatedAt = $datetime;
      return $this;
    }

    /**
     * [getCreateur]
     * @return string [nom des créateurs des projets]
     */
    public function getCreateur(): ?string {
        return $this->createur;
    }

    /**
     * [setCreateur]
     * @param string $createur
     * @return self
     */
    public function setCreateur(?string $createur): self {
        $this->createur = $createur;
        return $this;
    }

    /**
     * [getEnVitrine]
     * @return bool [Slider en vitrine ou non]
     */
    public function getEnVitrine(): ?bool {
        return $this->enVitrine;
    }

    /**
     * [setEnVitrine]
     * @param  int  $enVitrine
     * @return self
     */
    public function setEnVitrine(int $enVitrine): self {
        $this->enVitrine = $enVitrine;
        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection {
        return $this->tags;
    }

    /**
     * [addTag]
     * @param  Tags $tag
     * @return self
     */
    public function addTag(Tags $tag): self {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }
        return $this;
    }

    /**
     * [removeTag ]
     * @param  Tags $tag
     * @return self
     */
    public function removeTag(Tags $tag): self {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }
        return $this;
    }

    /**
     * [getSlider ]
     * @return string [nom du slider]
     */
    public function getSlider(): ?string {
        return $this->slider;
    }

    /**
     * [setSlider]
     * @param string $slider
     * @return self
     */
    public function setSlider(?string $slider): self {
        $this->slider = $slider;
        return $this;
    }

    /**
     * [getTexteSliderFr ]
     * @return string [texte des sliders en Fr]
     */
    public function getTexteSliderFr(): ?string {
        return $this->texteSliderFr;
    }

    /**
     * [setTexteSliderFr]
     * @param string $texteSliderFr
     * @return self
     */
    public function setTexteSliderFr(?string $texteSliderFr): self {
        $this->texteSliderFr = $texteSliderFr;
        return $this;
    }

    /**
     * [getTexteSliderEn ]
     * @return string [textes des sliders en En]
     */
    public function getTexteSliderEn(): ?string {
        return $this->texteSliderEn;
    }

    /**
     * [setTexteSliderEn]
     * @param string $texteSliderEn
     * @return self
     */
    public function setTexteSliderEn(?string $texteSliderEn): self {
        $this->texteSliderEn = $texteSliderEn;
        return $this;
    }

    /**
     * [setSliderFile]
     * @param File $slider
     */
    public function setSliderFile(File $slider = null) {
        $this->sliderFile = $slider;
        if ($slider) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * [getSliderFile]
     * @return File $slider
     */
    public function getSliderFile() {
        return $this->sliderFile;
    }

    /**
     * [getSlugFr - slugifie les noms des projets en Fr]
     * @return string [slug de nomfr]
     */
    public function getSlugFr(){
        $slugify = new Slugify();
        return $slugify->slugify($this->nomFr);
    }

    /**
     * [getSlugEn - slugifie les noms des projets en En]
     * @return string [slug de nomEn]
     */
    public function getSlugEn(){
        $slugify = new Slugify();
        return $slugify->slugify($this->nomEn);
    }

}
