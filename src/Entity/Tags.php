<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagsRepository")
 */
class Tags
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank
     */
    private $nomFr;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank
     */
    private $nomEn;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Projets", mappedBy="tags")
     */
    private $projets;

    /**
     * [private Slug du titre du menu en Fr]
     * @var [type="text"]
     */
    private $slugFr;

    /**
     * [private Slug du titre du menu en En]
     * @var [type="text"]
     */
    private $slugEn;

    /**
     * [__construct]
     */
    public function __construct() {
        $this->projets = new ArrayCollection();
    }

    /**
     * [__toString]
     * @return string [nomFr]
     */
    public function __toString() {
  		return $this->nomFr;
  	}

    /**
     * [getId]
     * @return int [Id des tags]
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * [getNomFr]
     * @return string [Noms des tags en Fr]
     */
    public function getNomFr(): ?string {
        return $this->nomFr;
    }

    /**
     * [setNomFr]
     * @param  string $nomFr
     * @return self
     */
    public function setNomFr(string $nomFr): self {
        $this->nomFr = $nomFr;
        return $this;
    }

    /**
     * [getNomEn]
     * @return string [Noms des tags en En]
     */
    public function getNomEn(): ?string {
        return $this->nomEn;
    }

    /**
     * [setNomEn]
     * @param  string $nomEn
     * @return self
     */
    public function setNomEn(string $nomEn): self {
        $this->nomEn = $nomEn;
        return $this;
    }

    /**
     * @return Collection|Projets[]
     */
    public function getProjets(): Collection {
        return $this->projets;
    }

    /**
     * [addProjet]
     * @param  Projets $projet
     * @return self
     */
    public function addProjet(Projets $projet): self {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->addTag($this);
        }
        return $this;
    }

    /**
     * [removeProjet]
     * @param  Projets $projet
     * @return self
     */
    public function removeProjet(Projets $projet): self {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            $projet->removeTag($this);
        }
        return $this;
    }

    /**
     * [getSlugFr - slugifie les noms des tags en Fr]
     * @return string [slug de nomFr]
     */
    public function getSlugFr(){
        $slugify = new Slugify();
        return $slugify->slugify($this->nomFr);
    }

    /**
     * [getSlugEn - slugifie les noms des tags en Fr]
     * @return string [slug de nomEn]
     */
    public function getSlugEn(){
        $slugify = new Slugify();
        return $slugify->slugify($this->nomEn);
    }
}
