<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nomFr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nomEn;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Posts", mappedBy="categorie")
     */
    private $posts;

    /**
     * [private Slug du titre du menu en Fr]
     * @var [type="text"]
     */
    private $slugFr;

    /**
     * [private Slug du titre du menu en En]
     * @var [type="text"]
     */
    private $slugEn;

    /**
     * [__construct]
     */
    public function __construct() {
        $this->posts = new ArrayCollection();
    }

    /**
     * [__toString]
     * @return string [nomFr]
     */
    public function __toString(){
  		return $this->nomFr;
  	}

    /**
     * [getId]
     * @return int [id des catégories]
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * [getNomFr]
     * @return string [nom des catégories en Fr]
     */
    public function getNomFr(): ?string {
        return $this->nomFr;
    }

    /**
     * [setNomFr]
     * @param  string $nomFr
     * @return self
     */
    public function setNomFr(string $nomFr): self {
        $this->nomFr = $nomFr;
        return $this;
    }

    /**
     * [getNomEn]
     * @return string [nom des catégories en En]
     */
    public function getNomEn(): ?string {
        return $this->nomEn;
    }

    /**
     * [setNomEn]
     * @param  string $nomEn
     * @return self
     */
    public function setNomEn(string $nomEn): self{
        $this->nomEn = $nomEn;
        return $this;
    }

    /**
     * @return Collection|Posts[]
     */
    public function getPosts(): Collection {
        return $this->posts;
    }

    /**
     * [addPost - ajout d'un post]
     * @param  Posts $post
     * @return self
     */
    public function addPost(Posts $post): self {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setCategorie($this);
        }
        return $this;
    }

    /**
     * [removePost - remove d'un post]
     * @param  Posts $post
     * @return self
     */
    public function removePost(Posts $post): self {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getCategorie() === $this) {
                $post->setCategorie(null);
            }
        }
        return $this;
    }

    /**
     * [getSlugFr - slugifie les noms des catégories en Fr]
     * @return string [slug de nomFr]
     */
    public function getSlugFr(){
       $slugify = new Slugify();
       return $slugify->slugify($this->nomFr);
    }

    /**
     * [getSlugFr - slugifie les noms des catégories en En]
     * @return string [slug de nomEn]
     */
    public function getSlugEn(){
       $slugify = new Slugify();
       return $slugify->slugify($this->nomEn);
    }
}
