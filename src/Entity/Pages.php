<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PagesRepository")
 */
class Pages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titreMenuFr;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titreMenuEn;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titreFr;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titreEn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreDescriptionFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreDescriptionEn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sousTitreDescriptionFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sousTitreDescriptionEn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionEn;

    /**
     * @ORM\Column(type="integer")
     */
    private $tri;

    /**
     * [private Slug du titre du menu en Fr]
     * @var [type="text"]
     */
    private $slugFr;

    /**
     * [private Slug du titre du menu en En]
     * @var [type="text"]
     */
    private $slugEn;

    /**
     * [getId]
     * @return int [id des pages]
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * [getTitreMenuFr]
     * @return string [titres du menu en Fr]
     */
    public function getTitreMenuFr(): ?string {
        return $this->titreMenuFr;
    }

    /**
     * [setTitreMenuFr]
     * @param string $titreMenuFr
     * @return self
     */
    public function setTitreMenuFr(string $titreMenuFr): self {
        $this->titreMenuFr = $titreMenuFr;
        return $this;
    }

    /**
     * [getTitreMenuEn]
     * @return string [titres du menu en En]
     */
    public function getTitreMenuEn(): ?string {
        return $this->titreMenuEn;
    }

    /**
     * [setTitreMenuEn]
     * @param string $titreMenuEn
     * @return self
     */
    public function setTitreMenuEn(string $titreMenuEn): self {
        $this->titreMenuEn = $titreMenuEn;
        return $this;
    }

    /**
     * [getTitreFr]
     * @return string [titre de la page en Fr]
     */
    public function getTitreFr(): ?string {
        return $this->titreFr;
    }

    /**
     * [setTitreFr]
     * @param string $titreFr
     * @return self
     */
    public function setTitreFr(string $titreFr): self {
        $this->titreFr = $titreFr;
        return $this;
    }

    /**
     * [getTitreEn]
     * @return string [titre de la page en En]
     */
    public function getTitreEn(): ?string {
        return $this->titreEn;
    }

    /**
     * [setTitreEn]
     * @param string $titreEn
     * @return self
     */
    public function setTitreEn(string $titreEn): self {
        $this->titreEn = $titreEn;
        return $this;
    }

    /**
     * [getTitreDescriptionFr]
     * @return string [titre de la description en Fr]
     */
    public function getTitreDescriptionFr(): ?string {
        return $this->titreDescriptionFr;
    }

    /**
     * [setTitreDescriptionFr]
     * @param string $titreDescriptionFr
     * @return self
     */
    public function setTitreDescriptionFr(?string $titreDescriptionFr): self {
        $this->titreDescriptionFr = $titreDescriptionFr;
        return $this;
    }

    /**
     * [getTitreDescriptionEn]
     * @return string [titre de la description en En]
     */
    public function getTitreDescriptionEn(): ?string {
        return $this->titreDescriptionEn;
    }

    /**
     * [setTitreDescriptionEn]
     * @param string $titreDescriptionEn
     * @return self
     */
    public function setTitreDescriptionEn(?string $titreDescriptionEn): self {
        $this->titreDescriptionEn = $titreDescriptionEn;
        return $this;
    }

    /**
     * [getSousTitreDescriptionFr]
     * @return string [sous titre de la description en fr]
     */
    public function getSousTitreDescriptionFr(): ?string {
        return $this->sousTitreDescriptionFr;
    }

    /**
     * [setSousTitreDescriptionFr]
     * @param string $sousTitreDescriptionFr
     * @return self
     */
    public function setSousTitreDescriptionFr(?string $sousTitreDescriptionFr): self {
        $this->sousTitreDescriptionFr = $sousTitreDescriptionFr;
        return $this;
    }

    /**
     * [getSousTitreDescriptionEn]
     * @return string [sous titre de la description en En]
     */
    public function getSousTitreDescriptionEn(): ?string {
        return $this->sousTitreDescriptionEn;
    }

    /**
     * [setSousTitreDescriptionEn]
     * @param string $sousTitreDescriptionEn
     * @return self
     */
    public function setSousTitreDescriptionEn(?string $sousTitreDescriptionEn): self {
        $this->sousTitreDescriptionEn = $sousTitreDescriptionEn;
        return $this;
    }

    /**
     * [getDescriptionFr]
     * @return string [description en Fr]
     */
    public function getDescriptionFr(): ?string {
        return $this->descriptionFr;
    }

    /**
     * [setDescriptionFr]
     * @param string $descriptionFr
     * @return self
     */
    public function setDescriptionFr(?string $descriptionFr): self {
        $this->descriptionFr = $descriptionFr;
        return $this;
    }

    /**
     * [getDescriptionEn]
     * @return string [description en En]
     */
    public function getDescriptionEn(): ?string {
        return $this->descriptionEn;
    }

    /**
     * [setDescriptionEn]
     * @param string $descriptionEn
     * @return self
     */
    public function setDescriptionEn(?string $descriptionEn): self {
        $this->descriptionEn = $descriptionEn;
        return $this;
    }

    /**
     * [getTri]
     * @return int [Tri des pages pour l'affichage]
     */
    public function getTri(): ?int {
        return $this->tri;
    }

    /**
     * [setTri]
     * @param  int  $tri
     * @return self
     */
    public function setTri(int $tri): self {
        $this->tri = $tri;
        return $this;
    }

    /**
     * [getSlugFr - slugifie les noms des pages en Fr]
     * @return string [slug de titreMenuFr]
     */
    public function getSlugFr(){
       $slugify = new Slugify();
       return $slugify->slugify($this->titreMenuFr);
    }

    /**
     * [getSlugEn - slugifie les noms des pages en En]
     * @return string [slug de titreMenuEn]
     */
    public function getSlugEn(){
       $slugify = new Slugify();
       return $slugify->slugify($this->titreMenuEn);
    }
}
