<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PostsRepository")
 * @Vich\Uploadable
 */
class Posts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $titreFr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $titreEn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreTexteLeadFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreTexteLeadEn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteLeadFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteLeadEn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreTexteSuiteFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titreTexteSuiteEn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteSuiteFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteSuiteEn;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="post_images", fileNameProperty="image")
     * @Assert\Image(minWidth = 1000, maxWidth = 1000, minHeight = 600, maxHeight = 600)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * [private Slug du titre du menu en Fr]
     * @var [type="text"]
     */
    private $slugFr;

    /**
     * [private Slug du titre du menu en En]
     * @var [type="text"]
     */
    private $slugEn;

    /**
     * [__construct]
     */
    public function __construct() {
        $this->dateCreation = new \Datetime();
    }

    /**
     * [__toString - Appel par easyadminBundle]
     * @return string [titreFr]
     */
    public function __toString(){
  		return $this->titreFr;
  	}

    /**
     * [getId]
     * @return int [id des posts]
     */
    public function getId(): ?int{
        return $this->id;
    }

    /**
     * [getTitreFr]
     * @return string [titre des posts en Fr]
     */
    public function getTitreFr(): ?string {
        return $this->titreFr;
    }

    /**
     * [setTitreFr]
     * @param  string $titreFr
     * @return self
     */
    public function setTitreFr(string $titreFr): self {
        $this->titreFr = $titreFr;
        return $this;
    }

    /**
     * [getTitreEn]
     * @return string [titre des posts en En]
     */
    public function getTitreEn(): ?string {
        return $this->titreEn;
    }

    /**
     * [setTitreEn]
     * @param  string $titreEn
     * @return self
     */
    public function setTitreEn(string $titreEn): self {
        $this->titreEn = $titreEn;

        return $this;
    }

    /**
     * [getTitreTexteLeadFr]
     * @return string [titre des textes lead en Fr]
     */
    public function getTitreTexteLeadFr(): ?string {
        return $this->titreTexteLeadFr;
    }

    /**
     * [setTitreTexteLeadFr]
     * @param string $titreTexteLeadFr
     * @return self
     */
    public function setTitreTexteLeadFr(?string $titreTexteLeadFr): self {
        $this->titreTexteLeadFr = $titreTexteLeadFr;
        return $this;
    }

    /**
     * [getTitreTexteLeadEn]
     * @return string [titre des textes lead en En]
     */
    public function getTitreTexteLeadEn(): ?string {
        return $this->titreTexteLeadEn;
    }

    /**
     * [setTitreTexteLeadEn]
     * @param string $titreTexteLeadEn
     * @return self
     */
    public function setTitreTexteLeadEn(?string $titreTexteLeadEn): self {
        $this->titreTexteLeadEn = $titreTexteLeadEn;
        return $this;
    }

    /**
     * [getTexteLeadFr]
     * @return string [textes lead en Fr]
     */
    public function getTexteLeadFr(): ?string {
        return $this->texteLeadFr;
    }

    /**
     * [setTexteLeadFr]
     * @param string $texteLeadFr
     * @return self
     */
    public function setTexteLeadFr(?string $texteLeadFr): self {
        $this->texteLeadFr = $texteLeadFr;
        return $this;
    }

    /**
     * [getTexteLeadEn]
     * @return string [textes lead en En]
     */
    public function getTexteLeadEn(): ?string {
        return $this->texteLeadEn;
    }

    /**
     * [setTexteLeadEn]
     * @param string $texteLeadEn
     * @return self
     */
    public function setTexteLeadEn(?string $texteLeadEn): self {
        $this->texteLeadEn = $texteLeadEn;
        return $this;
    }

    /**
     * [getTitreTexteSuiteFr]
     * @return string [titres des textes suite en Fr]
     */
    public function getTitreTexteSuiteFr(): ?string {
        return $this->titreTexteSuiteFr;
    }

    /**
     * [setTitreTexteSuiteFr]
     * @param string $titreTexteSuiteFr
     * @return self
     */
    public function setTitreTexteSuiteFr(?string $titreTexteSuiteFr): self {
        $this->titreTexteSuiteFr = $titreTexteSuiteFr;
        return $this;
    }

    /**
     * [getTitreTexteSuiteEn]
     * @return string [titres des textes suite en En]
     */
    public function getTitreTexteSuiteEn(): ?string {
        return $this->titreTexteSuiteEn;
    }

    /**
     * [setTitreTexteSuiteEn]
     * @param string $titreTexteSuiteEn
     * @return self
     */
    public function setTitreTexteSuiteEn(?string $titreTexteSuiteEn): self {
        $this->titreTexteSuiteEn = $titreTexteSuiteEn;
        return $this;
    }

    /**
     * [getTexteSuiteFr]
     * @return string [textes suite en Fr]
     */
    public function getTexteSuiteFr(): ?string {
        return $this->texteSuiteFr;
    }

    /**
     * [setTexteSuiteFr]
     * @param string $texteSuiteFr
     * @return self
     */
    public function setTexteSuiteFr(?string $texteSuiteFr): self {
        $this->texteSuiteFr = $texteSuiteFr;
        return $this;
    }

    /**
     * [getTexteSuiteEn]
     * @return string [textes suite en En]
     */
    public function getTexteSuiteEn(): ?string {
        return $this->texteSuiteEn;
    }

    /**
     * [setTexteSuiteEn]
     * @param string $texteSuiteEn
     * @return self
     */
    public function setTexteSuiteEn(?string $texteSuiteEn): self {
        $this->texteSuiteEn = $texteSuiteEn;
        return $this;
    }

    /**
     * [setImage]
     * @param string $image [nom de l'image]
     */
    public function setImage($image) {
        $this->image = $image;
    }

    /**
     * [getImage]
     * @return string $image
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * [setImageFile]
     * @param File $imageFile
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * [getImageFile]
     * @return File $imageFile
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * [getUpdatedAt]
     * @return Datetime [date de l'upload des images]
     */
    public function getUpdatedAt() {
       return $this->updatedAt;
    }

    /**
     * [setUpdatedAt]
     * @param DateTime $datetime
     */
    public function setUpdatedAt(\DateTime $datetime) {
        $this->updatedAt = $datetime;
        return $this;
    }

    /**
     * [getDateCreation]
     * @return DateTime [date de creation]
     */
    public function getDateCreation(): ?\DateTimeInterface {
        return $this->dateCreation;
    }

    /**
     * [setDateCreation]
     * @param  DateTimeInterface $dateCreation
     * @return self
     */
    public function setDateCreation(\DateTimeInterface $dateCreation): self {
        $this->dateCreation = $dateCreation;
        return $this;
    }

    /**
     * [getCategorie]
     * @return Categorie [catégories des posts]
     */
    public function getCategorie(): ?Categories {
        return $this->categorie;
    }

    /**
     * [setCategorie]
     * @param  Categories $categorie
     * @return self
     */
    public function setCategorie(?Categories $categorie): self {
        $this->categorie = $categorie;
        return $this;
    }

    /**
     * [getSlugFr - slugifie les titres des posts en Fr]
     * @return string [slug de titreFr]
     */
    public function getSlugFr(){
       $slugify = new Slugify();
       return $slugify->slugify($this->titreFr);
    }

    /**
     * [getSlugEn - slugifie les titres des posts en En]
     * @return string [slug de titreEn]
     */
    public function getSlugEn(){
       $slugify = new Slugify();
       return $slugify->slugify($this->titreEn);
    }
}
