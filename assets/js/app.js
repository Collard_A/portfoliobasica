/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

$(function(){

    $('#moreWorks').click(function(e){
      e.preventDefault();

      var url = $(this).attr('data-url');
      var nbProjets = $('.projets-preview').length;

      $.ajax({
          url: url,
          method: 'post',
          data: {
            offset: nbProjets
          },
          success: function(reponseSrv){
            $('#listWorks').append(reponseSrv)
                            .find('.projets-preview:nth-last-of-type(-n+6)')
                            .hide().fadeIn();
          }
      });

    });

  });
